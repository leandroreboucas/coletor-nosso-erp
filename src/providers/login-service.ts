import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { Usuario } from './../pages/usuario/usuario.model';

@Injectable()
export class LoginService {

  constructor(public http: Http) {
  }

/*
  postData(parans) {
            let headers = new Headers({ 'Content-Type' : 'application/x-www-form-urlencoded' });
            return this.http.post(this.api + "apiCadastro.php", parans, {
                  headers:headers,
                  method:"POST"
            }).map(
                  (res:Response) => {return res.json();}
            );
      }
      */

  login(usuario: Usuario, baseUrl: string) {
    let headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("Content-Type", 'application/json');

    return new Promise((resolve, reject) => {
      this.http.post(baseUrl + '/api/login', JSON.stringify(usuario), {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, error => {
          reject(error);
        })
    });
  }

}

/*
let headers = new Headers();
headers.append("Content-Type", 'application/json');
headers.append("Cache-Control", 'no-store; no-cache; must-revalidate');

let options = new RequestOptions({
  method: RequestMethod.Post,
  url: "http://www.myserver.fr/folder/php/porte.php",
  headers: headers,
  body: JSON.stringify(trans)
});

return this.http.request(new Request(options))
  .map((res: Response) => {
    if (res) {
      return [{ status: res.status, json: res.json() }]
    }
  });
  */