import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {LoginPage} from '../pages/login/login';
import {ConfigurationPage} from '../pages/configuration/configuration';
import {TransferenciaPage} from './../pages/transferencia/transferencia';
import {InventarioPage} from './../pages/inventario/inventario';
import {SincronizarPage} from '../pages/sincronizar/sincronizar';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ConfigurationPage,
    TransferenciaPage,
    InventarioPage,
    SincronizarPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ConfigurationPage,
    TransferenciaPage,
    InventarioPage,
    SincronizarPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
