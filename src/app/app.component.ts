import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import {ConfigurationPage} from '../pages/configuration/configuration';
import {TransferenciaPage} from '../pages/transferencia/transferencia';
import {InventarioPage} from '../pages/inventario/inventario';
import {SincronizarPage} from '../pages/sincronizar/sincronizar';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage = ConfigurationPage;
  pages: Array<{title: string, component: any}>;

  openPage(page){
    this.rootPage = page.component;
  }

  constructor(platform: Platform) {
    platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();
      this.pages = [
        {title: 'Inventário', component: InventarioPage},
        {title: 'Transferência', component: TransferenciaPage},
        {title: 'Sincronizar', component: SincronizarPage},
        {title: 'Configuração', component: ConfigurationPage}
        ];
    });
  }
}
