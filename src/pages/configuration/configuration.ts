import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';

import { LoginService } from './../../providers/login-service';
import { Usuario } from './../usuario/usuario.model';
import { InventarioPage } from './../inventario/inventario';

@Component({
  selector: 'configuration',
  templateUrl: 'configuration.html',
  providers: [LoginService]
})
export class ConfigurationPage {

  private baseUrl: string;
  private user: Usuario;
  private obj: any;
  private loading: Loading;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private loginService: LoginService,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController) {

    this.baseUrl = 'http://192.168.0.104:8080/api-nossoerp';
    this.user = { usr_codigo: '', usr_login: '', usr_nome: '', usr_senha: '' };
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Autenticando...'
    });
    this.loading.present();
  }

  send(user: Usuario, baseUrl: string) {
    this.showLoading();
    this.loginService.login(user, baseUrl)
      .then(data => {
        this.obj = data;
        if (this.obj.usr_codigo) {
          window.localStorage.setItem('baseUrl', baseUrl);
          window.localStorage.setItem('user', JSON.stringify(this.obj));
          this.loading.dismiss();
          this.navCtrl.setRoot(InventarioPage);
        } else {
          this.loading.dismiss();
          let alert = this.alertCtrl.create({
            title: 'Autenticação',
            subTitle: 'Usuaário não encontrado',
            buttons: ['OK']
          });
          alert.present(prompt);
        }
      })
      .catch(error => {
        console.log(error);
        let alert = this.alertCtrl.create({
            title: 'Autenticação',
            subTitle: 'Erro ao conectar no webservice \n' + error,
            buttons: ['OK']
          });
          alert.present(prompt);
        this.loading.dismiss();
      });
  }

}
