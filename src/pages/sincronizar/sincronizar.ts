import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the Sincronizar page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-sincronizar',
  templateUrl: 'sincronizar.html'
})
export class SincronizarPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

}
